package com.example.demo.port;
import java.net.Socket;
import org.springframework.stereotype.Component;
import java.io.IOException;

@Component
public class Port{
	public static String isremoteport(String hostname,int portnum) {
		
		try {
			new Socket(hostname,portnum).close();
			return "port is active";
		}
		catch(IOException e){
			return "port is inactive";
		}
	}
}




