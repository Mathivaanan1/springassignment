package com.example.demo.Enter;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="PORT")
public class Enter {
 
	@Id
	private int id;
	private int ip;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIp() {
		return ip;
	}
		public void setIp(int ip) {
		this.ip = ip;
	}
		public Enter(int id, int ip) {
			super();
			this.id = id;
			this.ip = ip;
		}
		public Enter() {
			super();
			// TODO Auto-generated constructor stub
		}

	
	
}
