package com.example.demo.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


@Component()
@Service
public class SendMail {
 
	@Autowired
	private JavaMailSender mailSender;
	
	public void sendmail(String tomail,String subject,String body) {
		
		SimpleMailMessage msg=new SimpleMailMessage();
		
		//msg.setFrom("subramani832ak@gmail.com");
		msg.setTo(tomail);
		msg.setText(body);
		msg.setSubject(subject);
		
		mailSender.send(msg);
		
		System.out.println("Mail send successfully");
	}
	}
