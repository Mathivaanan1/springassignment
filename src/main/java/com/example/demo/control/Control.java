package com.example.demo.control;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Enter.Enter;
import com.example.demo.imp.Connect;
import com.example.demo.mail.SendMail;
//import com.example.demo.model.User;
import com.example.demo.port.Port;

@Controller
public class Control {

	@Autowired
	Connect con;
	
	@Autowired
	@Qualifier("sendMail")
	 private SendMail send;
	
	
	@RequestMapping("/")
	public String open() {
		
		return "main.jsp";
	}
	
//	@RequestMapping("create")
//	public String create() {
//		
//		return "index.jsp";
//	}
	@RequestMapping("addUser")
	@ResponseBody
	public String index(Enter e ) {
		con.save(e);
		return "added successfully";
	}
//	@RequestMapping("get")
//	public String get() {
//		return "findone.jsp";
//	}
	@RequestMapping("getone")
	@ResponseBody
	public Optional<Enter> get(int id) {
		return con.findById(id);
	}
	//@RequestMapping("find")
	//public String find() {
	//	return "findall.jsp";
	//}
	@RequestMapping("find")
	@ResponseBody
	public List<Enter> findall() {
		return con.findAll();
	}
	
	@RequestMapping("sts")
	@ResponseBody
	public String sts(int id) {
		int portnumber=id;
		String s2=String.valueOf(portnumber);
		String str=Port.isremoteport("localhost",id);
		String s3="port"+s2+":"+str;
		send.sendmail("mathivaanan44@gmail.com","port status", s3);
		
		return str;
				
	}
	
	
	
//	@RequestMapping("status/{id}")
//	@ResponseBody
//    public String status(@PathVariable("id") int id) {
//		int portnumber=id;
//		String s2=String.valueOf(portnumber);
//		String str=Port.isremoteport("localhost",id);
//		String s3="port"+s2+":"+str;
//		send.sendmail("mathivaanan44@gmail.com","port status", s3);
//		
//		return str;
//	}
	
	@RequestMapping("delete")
	@ResponseBody
	public String deleteall() {
		con.deleteAll();
		return"all port deleted";
	}
	
	
	
	@RequestMapping("findone/{id}")
	@ResponseBody
	public Optional<Enter> findone(@PathVariable("id") int id) {
		return con.findById(id);
	}
	
}
